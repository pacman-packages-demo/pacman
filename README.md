# pacman

Library-based package manager used by Arch Linux. [pacman](https://www.archlinux.org/packages/?q=pacman)

# Official documentation
## Man
* [*pacman* (8)](https://www.archlinux.org/pacman/pacman.8.html#_options)

# Semi-official documentation
* [*Comparison with other distros*
  ](https://wiki.alpinelinux.org/wiki/Comparison_with_other_distros)

# Unofficial documentation
* [*Enable Parallel Downloading In Pacman In Arch Linux*
  ](https://ostechnix.com/enable-parallel-downloading-in-pacman-in-arch-linux/)
  2021-06 Senthilkumar Palani (aka SK)
